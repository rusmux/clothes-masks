import json
import os
import pathlib
from itertools import islice
from typing import Iterable

import click
import fiftyone as fo
import numpy as np


def batched(iterable: Iterable, batch_size: int) -> Iterable:
    iterable = iter(iterable)
    while batch := tuple(islice(iterable, batch_size)):  # noqa: WPS332
        yield batch


def add_keypoints(sample: fo.Sample, keypoints_dir: pathlib.Path) -> None:
    filename = os.path.basename(sample.filepath)
    keypoints_path = (keypoints_dir / filename).with_suffix(".json")

    with open(keypoints_path) as keypoints_file:
        keypoints = json.load(keypoints_file)
        keypoints = keypoints["people"][0]
        keypoints.pop("person_id")

    fo_keypoints = []
    for label, points in keypoints.items():
        if not points:
            continue

        points = list(batched(points, 3))
        points = np.stack(points, dtype=np.float64)

        points[:, 0] /= sample.metadata.width
        points[:, 1] /= sample.metadata.height

        fo_keypoint = fo.Keypoint(
            label=label,
            points=points[:, [0, 1]],
            confidence=points[:, 2],
        )
        fo_keypoints.append(fo_keypoint)

    sample["keypoints"] = fo.Keypoints(keypoints=fo_keypoints)


def add_mask(sample: fo.Sample, masks_dir: pathlib.Path) -> None:
    filename = os.path.basename(sample.filepath)
    mask_path = (masks_dir / filename).with_suffix(".png")
    sample["segmentation"] = fo.Segmentation(mask_path=str(mask_path))


@click.command()
@click.option("--images_dir", type=click.Path(exists=True, file_okay=False), required=True)
@click.option("--keypoints_dir", type=click.Path(exists=True, file_okay=False), required=True)
@click.option("--masks_dir", type=click.Path(exists=True, file_okay=False), required=True)
def main(images_dir: str, keypoints_dir: str, masks_dir: str) -> None:
    keypoints_dir = pathlib.Path(keypoints_dir).resolve()
    masks_dir = pathlib.Path(masks_dir).resolve()

    dataset = fo.Dataset.from_images_dir(
        images_dir=images_dir,
        name="clothes",
    )
    dataset.stats(include_media=True)

    for sample in dataset.iter_samples(autosave=True):
        add_keypoints(sample, keypoints_dir)
        add_mask(sample, masks_dir)

    session = fo.launch_app(dataset, auto=False)
    session.open_tab()
    session.wait()


if __name__ == "__main__":
    main()
