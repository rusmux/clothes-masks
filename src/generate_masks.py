import pathlib

import click
import cv2
import numpy as np

from src.definitions import check_if_dir_exists

BLACK_COLOR = 0
WHITE_COLOR = 255
GRAY_COLOR = 128

SKIRT_COLOR = (128, 0, 64)
LEFT_LEG_COLOR = (0, 64, 128)
RIGHT_LEG_COLOR = (0, 64, 0)
LEFT_HAND_COLOR = (128, 128, 192)
RIGHT_HAND_COLOR = (128, 128, 64)
SHIRT_COLOR = (128, 0, 128)
ANKLE_COLOR = (0, 0, 64)


def generate_mask(mask_path: str) -> np.ndarray:
    clothing_mask = cv2.imread(mask_path)

    binary_mask = np.zeros_like(clothing_mask)

    for color in [SKIRT_COLOR, LEFT_LEG_COLOR, RIGHT_LEG_COLOR, ANKLE_COLOR]:
        binary_mask[(clothing_mask == color).all(axis=-1)] = WHITE_COLOR

    binary_mask = cv2.morphologyEx(binary_mask, cv2.MORPH_CLOSE, kernel=(3, 3), iterations=5)
    binary_mask = cv2.cvtColor(binary_mask, cv2.COLOR_BGR2GRAY)

    contours, _ = cv2.findContours(binary_mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    contour = max(contours, key=cv2.contourArea)
    hull = cv2.convexHull(contour)
    binary_mask = cv2.fillPoly(np.zeros_like(binary_mask), [hull], WHITE_COLOR)

    for color in [SHIRT_COLOR, LEFT_HAND_COLOR, RIGHT_HAND_COLOR]:  # noqa: WPS440
        binary_mask[(clothing_mask == color).all(axis=-1)] = BLACK_COLOR

    return binary_mask


@click.command()
@click.option("--images_dir", type=click.Path(exists=True, file_okay=False), required=True)
@click.option("--masks_dir", type=click.Path(exists=True, file_okay=False), required=True)
@click.option("--output_dir", type=click.Path(), required=True)
def main(images_dir: str, masks_dir: str, output_dir: str) -> None:
    check_if_dir_exists(output_dir)

    images_dir = pathlib.Path(images_dir)
    masks_dir = pathlib.Path(masks_dir)
    output_dir = pathlib.Path(output_dir)

    for image_path in images_dir.glob("*.jpg"):
        mask_path = (masks_dir / image_path.name).with_suffix(".png")
        binary_mask = generate_mask(str(mask_path))

        image = cv2.imread(str(image_path))
        image[binary_mask > BLACK_COLOR] = GRAY_COLOR

        output_path = str(output_dir / image_path.name)
        cv2.imwrite(output_path, image)


if __name__ == "__main__":
    main()
