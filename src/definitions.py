"""Global project definitions."""

import os
import pathlib
import shutil
import sys

import rich

console = rich.get_console()

PROJECT_DIR = pathlib.Path(__file__).parents[1].resolve()


def check_if_dir_exists(dir_path: str) -> None:
    if os.path.isdir(dir_path) and os.listdir(dir_path):
        overwrite = console.input(
            f'\nDirectory "{os.path.relpath(dir_path)}" is not empty. '  # noqa: WPS237
            r"Do you want to overwrite it? \[y/N] "  # noqa: WPS326
        )

        if not overwrite.lower().strip() == "y":
            console.print("Aborted.\n")
            sys.exit(0)

        shutil.rmtree(dir_path)
        console.print(f'Overwriting "{os.path.relpath(dir_path)}".\n')  # noqa: WPS237
    os.makedirs(dir_path, exist_ok=True)
