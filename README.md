# Generating masks

[![wemake-python-styleguide](https://img.shields.io/badge/style-wemake-000000.svg)](https://wemake-python-styleguide.readthedocs.io/en/latest/)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)

<img alt="original" height="300" src="sample/images/81007_00.jpg"/>
<img alt="processed" height="300" src="sample/processed/81007_00.jpg"/>

Create masks that cover lower body of the model.

# Infrastructure

The project uses [Poetry](https://python-poetry.org/docs) for dependency management. If there is no Poetry in the
system, you can install dependencies via `requirements.txt`. However, all dependencies must be added
through `poetry add <package>`. This way, all the sub-dependencies are pinned.

Data is versioned using [DVC](https://dvc.org) in S3 storage on the [Yandex Cloud](https://cloud.yandex.com)
platform. To add a dataset to DVC, use the `dvc add` and `dvc push` commands. You can download the object with
the `dvc pull` command.

The [wemake-python-styleguide](https://wemake-python-styleguide.readthedocs.io/en/latest) linter is used to control the
quality of the code. It is an extension for flake8. The linter setup is located in the `tox.ini` file. You can run the
linter with the command `flake8`. Also, [pre-commit](https://pre-commit.com) hooks are used for automatic formatting of
the code. You can run pre-commit manually with the command `pre-commit run --all-files`.

For continuous integration, GitLab CI is used.

# Setup

The command below will create a virtual environment for the project. If the system has conda or mamba,
the corresponding environment will be created, otherwise the `venv`-environment will be installed in the `.venv` folder.
When using conda, an environment with Python version 3.10 will be created by default. To install a different version,
change the value of the `PYTHON_VERSION` variable in the `Makefile`.

```Bash
make create_environment
```

After activating the virtual environment, to install all the necessary libraries, run the command:

```Bash
make install
```

# Data overview

Images, masks and keypoints can be pull from the DVC.

To browse the data run:

```Bash
python -m src.browse_dataset \
  --images_dir data/images \
  --keypoints_dir data/keypoints \
  --masks_dir data/masks
```

<img alt="FiftyOne" height="300" src="sample/other/fiftyone.jpeg"/>

# OpenCV approach

<img alt="original_mask" height="200" src="sample/masks/48362_00.png"/>
<img alt="binary_mask" height="200" src="sample/other/binary_mask_1.png"/>
<img alt="binary_mask" height="200" src="sample/other/binary_mask_2.png"/>
<img alt="binary_mask" height="200" src="sample/other/binary_mask_3.png"/>
<img alt="processed_image" height="200" src="sample/processed/48362_00.jpg"/>

- Binary mask is created that contains only the skirt and legs of the model.

- Closing morphological operation is applied to the mask to remove noise.

- Largest contour is found and used to a convex hull.

- Hands and shirt are removed from the hull.

# What didn't work

- Stable Diffusion Inpainting

More or less reasonable:

<img alt="generated" width="400" height="250" src="sample/other/stable_diffusion_1.jpeg"/>
<img alt="generated" width="400" height="250" src="sample/other/stable_diffusion_2.jpeg"/>

Not so good:

<img alt="generated" width="400" height="250" src="sample/other/stable_diffusion_3.jpeg"/>
<img alt="generated" width="400" height="250" src="sample/other/stable_diffusion_4.jpeg"/>
